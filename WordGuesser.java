import java.util.Scanner;

public class WordGuesser {

    // Constants
    public static final int MAXGUESSES = 6;

    /**
     * Check whether the inputs are alphabetic characters
     *
     * @param toCheck The string to check for only alphabetic characters
     * @return true if all characters are alphabetic, false otherwise
     */
    static boolean checkAllAlpha(String toCheck) {
        return toCheck.chars().allMatch(Character::isLetter);
    }

    /**
     * Check whether the word is of the specified length of 6 characters
     *
     * @param toCheck The string to check for length
     * @return true if the word is of the specified length, false otherwise
     */
    static boolean checkWordLength(String toCheck) {
        return toCheck.length() == 6;
    }

    public static void main(String[] args) {
        // initialize variables
        String secretWord;

        // initialize input
        Scanner input = new Scanner(System.in);

        // Prompt the user to enter the secret word until it's valid
        while (true) {
            System.out.println("Please enter the secret word:");
            secretWord = input.nextLine().toUpperCase(); // Convert to uppercase for case-insensitive comparison

            // Check if the word is valid
            if (checkAllAlpha(secretWord) && checkWordLength(secretWord)) {
                break; // Exit the loop if the word is valid
            } else {
                System.out.println("Invalid word provided. Word must be alphabetic and six characters long.");
            }
        }

        playGame(secretWord, input);
    }

    /**
     * Play the guessing game with the given secret word
     *
     * @param secretWord The secret word to guess
     * @param input      The Scanner object for user input
     */
    static void playGame(String secretWord, Scanner input) {
        int numGuesses = 0;

        while (numGuesses < MAXGUESSES) {
            System.out.println("Guess the secret word or type 'exit' to quit:");
            String guess = input.nextLine().toUpperCase(); // Convert to uppercase for case-insensitive comparison

            if (guess.equalsIgnoreCase("exit")) {
                System.out.println("Exiting the game!");
                break;
            }
            // Check if the guess is valid
            if (!checkAllAlpha(guess) || !checkWordLength(guess)) {
                System.out.println("Invalid guess. Guess must be alphabetic and six characters long.");
                continue;
            }

            // Check the guess and get the result
            int[] result = checkGuess(secretWord, guess);

            // Generate feedback
            String feedback = generateFeedback(secretWord, guess);

            // Output results and provide feedback to the user to help the user get closer to the secret word
            System.out.println("Feedback: " + feedback);
            System.out.println("Correct letters in correct positions: " + result[0]);
            System.out.println("Correct letters in incorrect positions: " + result[1]);
            System.out.println("Guesses left: " + (MAXGUESSES - numGuesses - 1));

            // Check if the guess is correct
            if (result[0] == secretWord.length()) {
                System.out.println("Congratulations! You've guessed the word: " + secretWord);
                return;
            }

            numGuesses++;
        }

        // End of game
        System.out.println("Sorry, you've run out of guesses. The secret word was: " + secretWord);
    }

    /**
     * Check if the guess matches the secret word and count correct positions and correct letters
     *
     * @param secretWord The secret word to guess
     * @param guess      The user's guess;
     * @return an array where the first element is the number of correct positions, and the second element is the number of correct letters
     */
    static int[] checkGuess(String secretWord, String guess) {
        int[] result = new int[2];
        int correctPos = 0;
        int correctLetters = 0;

        for (int i = 0; i < secretWord.length(); i++) {
            if (secretWord.charAt(i) == guess.charAt(i)) {
                correctPos++;
            } else if (secretWord.contains(String.valueOf(guess.charAt(i)))) {
                correctLetters++;
            }
        }

        result[0] = correctPos;
        result[1] = correctLetters;
        return result;
    }

    /**
     * Generate a string indicating the correct and incorrect positions of letters
     *
     * @param secretWord The secret word
     * @param guess      The user's guess;
     * @return a string indicating the correct and incorrect positions of letters
     */
    static String generateFeedback(String secretWord, String guess) {
        StringBuilder feedback = new StringBuilder();

        for (int i = 0; i < secretWord.length(); i++)
            if (secretWord.charAt(i) == guess.charAt(i)) {
                feedback.append(guess.charAt(i)); // Correct letter in the correct position
            } else if (secretWord.contains(String.valueOf(guess.charAt(i)))) {
                feedback.append("*"); // Correct letter in the wrong position
            } else {
                feedback.append("-"); // Incorrect letter
            }
        return feedback.toString();
    }
}